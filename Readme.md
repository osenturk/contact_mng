#Contact Manager

## Overview
The program composed of 3 main modules i.e. app, logs, and
migrations. The app module contains main and templates
packages. The overall configuration is located in config.py 
and the test configuration overwrites it via tests.py. 

Flaskforms are heavily used to speed up development. After
a form defined as a class, the template renders it as a neat
html automatically. For example: 'ContactForm' class.

Alembic and SQLAlchemy were used for data modelling and
migration. Flask shell is used via 'contact_manager.py' to 
test the functionality within unit tests by 'tests.py'.

Additional creation date field was added to support purge
function to filter out older records.  

Blueprints are used to support further expansion and to keep
the project highly cohesive and loosely coupled. Therefore,
factory patter applied whilst application initialization. 

The development was implemented on the 'dev' branch but there
might have been additional feature brunches for each 
functionality.

Your further questions, feel free to reach me out via my
email 'ozan.senturk@gmail.com'


##How to run:
open a command window and follow the steps below

```python
python3 -m venv venv

source venv/bin/activate

pip install -r requirements.txt

flask db init 

flask db migrate -m "contacts table"

flask db upgrade

flask run
```

Testing emails: flask shell

c = Contact.query.get(1)
e = Email(content='ozan.senturk@gmail.com', contact=c)

celery worker -A celery_worker.celery --loglevel=info   