from app import db
from datetime import datetime


class Contact(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), index=True, unique=True)
    first_name = db.Column(db.String(32))
    last_name = db.Column(db.String(32))
    phone = db.Column(db.String(11), index=True, unique=True)
    creation_time = db.Column(db.DateTime, default=datetime.utcnow)

    emails = db.relationship('Email', backref='contact', lazy='dynamic')

    def __repr__(self):
        return '<Contact {} with id: {}, first_name: {}, last_name: {}, ' \
               'phone: {}, emails: {}, creation:{}>' \
            .format(self.username, self.id, self.first_name, self.last_name,
                    self.phone, self.emails, self.creation_time)

class Email(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(240))
    contact_id = db.Column(db.Integer, db.ForeignKey('contact.id'))

    def __repr__(self):
        return '<Email {}>'.format(self.content)


