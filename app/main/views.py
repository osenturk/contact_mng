from flask import render_template, flash, redirect, url_for, request
from app import db
from app.main.forms import ContactForm, SearchForm, EmailForm
from app.models import Contact, Email
from app.main import bp
from app.main.tasks import example

@bp.route('/', methods=['GET', 'POST'])
@bp.route('/index', methods=['GET', 'POST'])
def index():
    return render_template('index.html', title='Home')

@bp.route('/add', methods=['GET', 'POST'])
def add():
    form = ContactForm()
    if form.validate():
        contact = Contact(username=form.username.data,
                          first_name=form.first_name.data,
                          last_name=form.last_name.data,
                          phone=form.phone.data)
        db.session.add(contact)
        db.session.commit()
        flash('Your contact is added!')
        return redirect(url_for('main.index'))

    return render_template('add.html', title='Home', form=form)

@bp.route('/find', methods=['GET', 'POST'])
def find():
    c = None
    form = SearchForm()
    if form.validate_on_submit():
        username = form.username.data
        c = Contact.query.filter_by(username=username).first()

    return render_template('find.html', form=form, contact=c)


@bp.route('/list_contacts', methods=['GET', 'POST'])
def list_contacts():
    contacts = Contact.query.limit(10).all()

    return render_template('list.html', contacts=contacts)

@bp.route('/edit_contact/<key>', methods=['GET', 'POST'])
def edit_contact(key):

    contact =Contact.query.filter_by(id=key).first()
    if contact is not None:
        form = ContactForm(formdata=request.form, obj=contact)

        if request.method == 'POST' and form.validate():
            contact.first_name = form.first_name.data
            contact.last_name = form.last_name.data
            contact.phone = form.phone.data

            db.session.commit()
            flash('Your contact is updated!')
            return redirect(url_for('main.edit_contact',key=key))

    return render_template('contact.html', form = form)

@bp.route('/delete_contact/<key>', methods=['GET', 'POST'])
def delete_contact(key):

    contact =Contact.query.filter_by(id=key).first()

    if contact is not None:
        db.session.delete(contact)
        db.session.commit()
        flash('Your contact was deleted!')
        redirect(url_for('main.list_contacts'))

    contacts = Contact.query.limit(10).all()

    return render_template('list.html', contacts=contacts)

@bp.route('/add_email/<key>', methods=['GET', 'POST'])
def add_email(key):

    contact = Contact.query.filter_by(id=key).first()
    if contact is not None:
        form = EmailForm()

        if form.validate_on_submit():
            email = Email(content=form.email.data,
                              contact_id=key)
            db.session.add(email)
            db.session.commit()
            flash('Your email was added!')
            return redirect(url_for('main.index'))

    return render_template('email.html', title='Home', form=form)


@bp.route('/task', methods=['GET', 'POST'])
def task():
    example.delay(100)
    return render_template('index.html', title='Home')

