from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, Length, Email
from wtforms import ValidationError
from ..models import Contact

class ContactForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    first_name = StringField('First name',
                             validators=[Length(min=0, max=32)])
    last_name = StringField('Last name',
                            validators=[Length(min=0, max=32)])
    phone = StringField('Phone',
                        validators=[Length(min=0, max=11)])

    submit = SubmitField('Add')

    def validate_contact(self, field):
        if Contact.query.filter_by(username=field.data).first():
            raise ValidationError('Contact already registered.')

class SearchForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    submit = SubmitField('Find')

class EmailForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Add')
