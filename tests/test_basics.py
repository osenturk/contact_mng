import unittest
from flask import current_app
from app import create_app, db
from datetime import timedelta
import datetime
from app import create_app, db
from app.models import Contact

class BasicsTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_app_exists(self):
        self.assertFalse(current_app is None)

    def test_app_is_testing(self):
        self.assertTrue(current_app.config['TESTING'])

    def test_contact_creation(self):
        c1 = Contact(username='john', first_name='John',
                    last_name='Hash', phone='1234567')

        c2 = Contact(username='Green', first_name='Fab',
                     last_name='Rica', phone='1234569')

        db.session.add_all([c1, c2])
        db.session.commit()

        contacts = Contact.query.all()

        self.assertEqual(len(contacts), 2)

    def test_purge_contact(self):
        since = datetime.datetime.now() - timedelta(minutes=1)

        contacts = Contact.query.filter(Contact.creation_time < since).all()

        self.assertLess(len(contacts),1)